# Introduction
This repo is merely meant to be a playground for me learning Zig.

## Build

```
zig build
```

## Run
```
zig build run
```

## Run tests
```
zig build test
```
